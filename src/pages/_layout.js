import React, { useEffect, useState } from 'react'
import Image from 'next/image'


export default function Layout({ children }) {

    const [isOpenMobileMenu, setIsOpenMobileMenu] = useState(false)

    useEffect(() => {

        console.log("Is Open Mobile Menu : ", isOpenMobileMenu);
    }, [isOpenMobileMenu])

    return (
        <div>
            <header className="absolute inset-x-0 top-0 z-50">
                <nav className="flex items-center justify-between p-6 lg:px-8" aria-label="Global">
                    <div className="flex lg:flex-1">
                        <a href="#" className="-m-1.5 p-1.5">
                            <span className="sr-only">Your Company</span>
                            <Image className="h-8 w-auto text-indigo-700" width={40} height={15} src="https://tailwindui.com/img/logos/mark.svg" alt="" />
                        </a>
                    </div>
                    <div className="flex lg:hidden">
                        <button type="button" onClick={() => setIsOpenMobileMenu(true)} className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700">
                            <span className="sr-only">Open main menu</span>
                            <Image src={'/img/open.svg'} width={20} height={20} alt='Open Main Menu' />
                        </button>
                    </div>
                    <div className="hidden lg:flex lg:gap-x-12">
                        <a href="#" className="text-sm font-semibold leading-6 text-gray-900">Product</a>
                        <a href="#" className="text-sm font-semibold leading-6 text-gray-900">Features</a>
                        <a href="#" className="text-sm font-semibold leading-6 text-gray-900">Marketplace</a>
                        <a href="#" className="text-sm font-semibold leading-6 text-gray-900">Company</a>
                    </div>
                    <div className="hidden lg:flex lg:flex-1 lg:justify-end">
                        <a href="#" className="text-sm font-semibold leading-6 text-gray-900">Log in <span aria-hidden="true">&rarr;</span></a>
                    </div>
                </nav>

                <div className={`lg:hidden transition-all ease-linear duration-700 ${isOpenMobileMenu ? ' opacity-100 ' : ' opacity-0 '} `} role="dialog" aria-modal="true">

                    <div className="fixed inset-0 z-50"></div>
                    <div className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10 ">
                        <div className="flex items-center justify-between">
                            <a href="#" className="-m-1.5 p-1.5">
                                <span className="sr-only">Your Company</span>
                                <Image className="h-8 w-auto" width={40} height={15} src="https://tailwindui.com/img/logos/mark.svg" alt="" />
                            </a>
                            <button type="button" onClick={() => setIsOpenMobileMenu(!isOpenMobileMenu)} className="-m-2.5 rounded-md p-2.5 text-gray-700">
                                <span className="sr-only">Close menu</span>
                                <Image width={20} height={20} src={'/img/close.svg'} alt='CLose Menu' />
                            </button>
                        </div>
                        <div className="mt-6 flow-root">
                            <div className="-my-6 divide-y divide-gray-500/10">
                                <div className="space-y-2 py-6">
                                    <a href="#" className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Product</a>
                                    <a href="#" className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Features</a>
                                    <a href="#" className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Marketplace</a>
                                    <a href="#" className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Company</a>
                                </div>
                                <div className="py-6">
                                    <a href="#" className="-mx-3 block rounded-lg px-3 py-2.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Log in</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className='absolute w-full h-2/4 bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 '></div>
            <main
                className={`flex min-h-screen flex-col items-center justify-between  p-24 bg-white`}
            >
                {children}
            </main>
        </div>
    )
}