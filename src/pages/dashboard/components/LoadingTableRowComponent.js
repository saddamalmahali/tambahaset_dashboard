import React from 'react'

function LoadingTableRowComponent() {
    return (
        <tr className=' animate-pulse'>
            <td className='border-b p-5 text-center'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
            <td className='border-b p-5'>
                <div className='h-3 bg-slate-200 rounded'></div>
            </td>
        </tr>
    )
}

export default LoadingTableRowComponent