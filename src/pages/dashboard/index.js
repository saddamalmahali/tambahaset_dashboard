"use server"
import React, { useEffect, useState } from 'react'

import Head from 'next/head';

import axios from 'axios'
import Image from 'next/image'
import moment from 'moment/moment'
import LoadingTableRowComponent from '@/pages/dashboard/components/LoadingTableRowComponent'
import Layout from '@/pages/_layout';

function Dashboard() {
    const [isLoading, setIsLoading] = useState(0)
    const [data, setData] = useState(null)
    const [error, setError] = useState('')
    const [totalPage, setTotalPage] = useState(0)
    const [total, setTotal] = useState(0)
    const [offset, setOffset] = useState(0)
    const [page, setPage] = useState(0)
    const [limit, setLimit] = useState(0)

    useEffect(() => {
        setPage(1);
        console.log("use Effect");
        loadingData();
    }, []);

    const loadingData = () => {
        setIsLoading(true)

        let queryString = require('querystring')

        let stringFilter = {

        };

        stringFilter = queryString.encode(stringFilter);

        axios.get(`http://tambahasetapi.devadam.my.id/v1/coins/get_all_coins?${stringFilter}`).then((response) => {
            setData(response.data.data)
            setTotal(response.data.total)
            setOffset(response.data.skip)
            setPage(response.data.page)
            setTotalPage(response.data.total_page)
            setLimit(response.data.limit)
            setIsLoading(false)
        }).catch((error) => {
            setError(error.response);
            setIsLoading(false)
        })
    }

    const getData = (pageData) => {
        setIsLoading(true)
        axios.get(`http://tambahasetapi.devadam.my.id/v1/coins/get_all_coins?page=${pageData}`).then((response) => {
            setData(response.data.data)
            setTotal(response.data.total)
            setOffset(response.data.skip)
            setPage(response.data.page)
            setTotalPage(response.data.total_page)
            setLimit(response.data.limit)
            setIsLoading(false)
        }).catch((error) => {
            setError(error.response);
            setIsLoading(false)
        })
    }

    const nextData = () => {
        // if (totalPage <= page) {
        let pageNext = parseInt(page) + 1
        console.log("Page Next : ", pageNext)
        getData(pageNext);
        // }
    }

    const prevData = () => {
        if (page > 1) {
            let pagePrev = page - 1;
            console.log("Page Prev : ", pagePrev)
            getData(pagePrev);
        }
    }

    return (
        <Layout>
            <Head>
                <title>Dashboard Coin</title>
            </Head>

            <div className='w-3/4'>
                <header id="header" className="relative mb-10">
                    <div>
                        <p className="mb-2 text-sm leading-6 font-semibold text-white dark:text-white">Table History Coin</p>
                        <div className="flex items-center">
                            <h1 className="inline-block text-2xl sm:text-3xl font-extrabold text-slate-900 tracking-tight dark:text-slate-200">History Coin</h1>
                        </div>
                    </div>
                    <p className="mt-2 text-lg text-slate-700 dark:text-slate-400">History Coin for calculate transaction.</p>
                </header>
                <div className='not-prose relative bg-slate-50 rounded-xl overflow-hidden dark:bg-slate-800/25 shadow-lg'>
                    <div className='absolute  inset-0 bg-grid-slate-100 [mask-image:linear-gradient(0deg,#fff,rgba(255,255,255,0.6))] dark:bg-grid-slate-700/25 dark:[mask-image:linear-gradient(0deg,rgba(255,255,255,0.1),rgba(255,255,255,0.5))]' style={{ backgroundPosition: '10px 10px' }}></div>
                    <div className='relative rounded-xl overflow-auto'>
                        <div className='shadow-sm overflow-hidden my-8'>
                            <table className={'border-collapse table-auto w-full text-sm'}>
                                <thead>
                                    <tr>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Symbol</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Exchange</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Indicator</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Interval</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Value</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Status</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Date Fetch</th>
                                        <th className='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-center'>Actions</th>
                                    </tr>
                                </thead>
                                <tbody className='bg-white dark:bg-slate-800'>
                                    {isLoading ? (
                                        <React.Fragment>
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                            <LoadingTableRowComponent />
                                        </React.Fragment>

                                    ) : data && data.map((item, index) => {
                                        let status = (
                                            <span className='p3 bg-red-600/100 text-white px-2 py-1 rounded text-xs'>
                                                Non Active
                                            </span>
                                        )

                                        if (item.status === "1") {
                                            status = (
                                                <span className='p3 bg-green-600/100 text-white px-2 py-1 rounded text-xs'>
                                                    Active
                                                </span>
                                            )
                                        }

                                        return (
                                            <tr key={index.toString()}>

                                                {/* Symbol Column */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>{item.symbol}</td>
                                                {/* Exchange Column  */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>{item.exchange}</td>
                                                {/* Indicator Column */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>{item.indicator}</td>
                                                {/* Interval Column */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>{item.interval}</td>
                                                {/* Value Column */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>{item.value}</td>
                                                {/* Status Column  */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>{status}</td>
                                                {/* Date Column  */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>
                                                    <div className='flex flex-row'>
                                                        <Image src='/img/calendar.svg' alt='Calendar Logo' width="15" height="15" className='mr-2' />
                                                        <span className='font-mono'> {moment(item.date_created).format("DD-MM-YYYY")}</span>
                                                    </div>
                                                    <div className='flex flex-row'>
                                                        <Image src='/img/clock.svg' alt='Calendar Logo' width="15" height="15" className='mr-2' />
                                                        <span className='font-mono'> {moment(item.date_created).format("hh:mm")}</span>
                                                    </div>
                                                </td>
                                                {/* Action Column  */}
                                                <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'></td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>

                        <div className="flex flex-row w-full items-center justify-between mb-2 px-2">
                            <span className="text-sm text-gray-700 dark:text-gray-400 mr-2">
                                Showing <span className="font-semibold text-gray-900 dark:text-white">{offset + 1}</span> to <span className="font-semibold text-gray-900 dark:text-white">{offset + limit}</span> of <span className="font-semibold text-gray-900 dark:text-white">{total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span> Entries
                            </span>
                            <div className="inline-flex xs:mt-0">
                                <button onClick={() => prevData()} className={`inline-flex items-center px-4 py-2 text-sm font-medium text-white ${page > 1 ? 'bg-gray-800 hover:bg-gray-900 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white' : 'bg-gray-600 dark:bg-gray-600 dark:border-gray-500 dark:text-gray-400 dark:hover:text-white'}  rounded-l  `}>
                                    {/* <Image src={'/img/arrow_left.svg'} alt='Arrow Left Icon' width={10} height={10} className='text-white ' /> */}
                                    Prev
                                </button>
                                <button onClick={() => nextData()} className="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-gray-800 border-0 border-l border-gray-700 rounded-r hover:bg-gray-900 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                    Next
                                    {/* <Image src={'/img/arrow_right.svg'} alt='Arrow Right Icon' width={10} height={10} className='text-white' /> */}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Dashboard